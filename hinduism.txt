HINDUISM

# Who is identified as the founder of Hinduism?

[x] It has no one identifiable founder.
[ ] Atman
[ ] Brahman
[ ] vishnu

# The earliest, ancient scriptures of India are known as what?

[x] the Vedas
[ ] the Bhagavad Gita
[ ] the Upanishads
[ ] the Rig Veda


# The core of the Vedas, the earliest Hindu sacred literature, consists of what?

[x] The written record of sacred chants.
[ ] An epic poem recounting history
[ ] Philosophical and religious ideas which emerged from introsepction and meditation
[ ] The words of the Aryan gods


# The name 'Vedas' means what?

[x] "knowledge" or "sacred lore"
[ ] "deepest spirit"
[ ] "the wheel of life"
[ ] "complete freedom"


# Which is not one of the four basic sacred text collections which constitute the Vedas?

[ ] Rig Veda
[ ] Yajur Veda
[x] Aranyakas Veda
[ ] Artharva Veda


# The Rig Veda is comprised of what?

[x] more than a thousand chants to the Aryan gods
[ ] ceremonial knowledge
[ ] musical elaborations of Vedic chants
[ ] practical prayers and charms


# The Yajur Veda is comprised of what?

[x] ceremonial knowledge
[ ] practical prayers and charms
[ ] musical elaborations of Vedic chants
[ ] more than a thousand chants to the Aryan gods


# The Sama Veda is comprised of what?

[x] musical elaborations of Vedic chants
[ ] ceremonial knowledge
[ ] more than a thousand chants to the Aryan gods
[ ] practical prayers and charms


# The Artharva Veda consists of what?

[x] practical prayers and charms
[ ] musical elaborations of Vedic chants
[ ] ceremonial knowledge
[ ] more than a thousand chants to the Aryan gods


# Which is considered the most important collection which make up the Vedas?

[x] the Rig Veda
[ ] the Yajur Veda
[ ] the Sama Veda
[ ] the Artharva Veda


# Which of the Vedas has an account of the origin of the universe?

[x] the Rig Veda
[ ] the Yajur Veda
[ ] the Sama Veda
[ ] the Artharva Veda


# Which is not also referred to by the term Vedas in colloquial use?

[ ] Brahmanas
[ ] Aranyakas
[ ] Upanishads
[x] the Bhagavad Gita


# What does the Brahamanas not detail?

[ ] proper time and place for ceremonies
[ ] preparation of the ground
[ ] purification rites
[x] exegesis of the rituals for ascetics


# What is explained in the Aranyakas?

[x] nonliteral, symbolic understanding and practice of rituals for ascetics
[ ] proper time and place for ceremonies
[ ] philosophical and religious ideas which arose in introspective and meditative traidtions
[ ] purification rites


# What is detailed in the Upanishads?

[x] philosophical and religious ideas which arose in introspective and meditative traditions.
[ ] proper time and place for ceremonies
[ ] nonliteral, symbolic understanding and practice of ritals for ascetics
[ ] proper time and place for ceremonies


# What is a primary theme in the Upanishads?

[x] With spiritual discipline and meditation, both priests and nonpriests can experience the spiritual reality that underlies all seemingly seperate realities.
[ ] The story of how the sons of Pandu conquered their cousincs, the Kauravas
[ ] The worship of Aryan gods who control the forces of nature
[ ] A collection of instructions for proper worship and devotion


# In what way does the Upanishads differ from the earlier Vedic material?

[x] Anyone with the necessary experience can be a spiritual master.
[ ] Only dedicated priests and gurus can be spiritual masters 
[ ] It held that Brahman could not be known
[ ] It advocated the use of violence.


# In the early Vedic texts, what did the term Brahman originally refer to?

[x] the cosmic power present in the Vedic sacrifice and chants, over which the priest had control.
[ ] the Aryan gods who controled the forces of nature
[ ] the divine reality at the heart of things
[ ] the priest caste


# How did the Upanishads expand the sense of the term Brahman?

[x] It understood Brahman as the divine reality at the heart of things
[ ] It understood Brahman as the realm of the gods
[ ] It understood Brahman as the divine will
[ ] It understood Brahman as karma


# How do the Upanishads characterize Brahman, the Divine Spirit?

[x] it is real and may be directly known.
[ ] it is abstract but may be directly known.
[ ] it is abstract and cannot be directly known.
[ ] it is real but cannot be directly known.


# Which is not a description of how the Upanishads characterize what it is like to know Brahman?

[ ] the lived experience that all things are in some way holy because they come from the same sacred source
[ ] all things are in some way ultimately one
[ ] recognition of many unities; all apparent seperations and divisions blur
[x] perception of the falsehoods of the world of maya


# What is not a description of the nature of Brahman as perceived by the knower?

[ ] sat, reality itself
[ ] chit, pure consciousness
[ ] ananda, bliss
[x] atman, deepest self


# How does the Upanishads categorize the concepts of Brahman and Atman?

[x] Brahman refers to the experience of the sacred within nature and the external universe while Atman refers to the experience of the sacred within oneself.
[ ] Brahman refers to the experience of the sacred within oneself while Atman refers to the experience of the sacred within nature and the external universe
[ ] Brahman refers to the divine reality while Atman refers to the illusion of the everyday world
[ ] Brahman refers to the divine spirit while Atman refers to samasara, the wheel of life.

# What does the Upanishads mean when it speaks of the everyday world as maya.

[x] The world is real, but not in quite the way most people assume.
[ ] The world does not exist
[ ] The world is false perception
[ ] The world is deception


# What determines the direction one's rebirth?

[x] the automatic moral consequences of one's actions
[ ] the will of God or Brahman
[ ] the degree of devotion and faith practiced in one's life
[ ] a revelation of the nature of maya


# What is not a description of Moksha in the Upanishads?

[ ] the ultimate human goal
[ ] getting beyyond egoistic responses, such as resentment and anger, which limit the individual
[ ] liberation from even being an individual
[x] the ultimate state of enlightenment


# The Bhagavad Gita is part of what?

[x] the Mahabharata
[ ] the Upanishads
[ ] the Vedas
[ ] the Brahman


# Why has the Bhagavad Gita posed a moral problem for some followers of Hinduism?

[x] It appears to permit violence contrary to the strong tradition of non-violence
[ ] It helped establish the caste system
[ ] It advocated worship of many deities
[ ] It reinterpreted the nature of Brahman


# The caste system was mentioned first in which?

[x] the Rei Veda
[ ] the Bhagavad Gita
[ ] the Upanishads
[ ] the Yajur Veda


# How can an individual change castes?

[ ] Changing work
[ ] Intermarrying
[x] Only through rebirth
[ ] They cannot


# What is considered the highest of life goals?

[x] moksha, complete freedom
[ ] kama, pleasure
[ ] artha, economic security and power
[ ] dharma, social and religious duty


# Which is not a source of issues that moderate Hinduism faces today?

[ ] The conservative social teachings of traditional Hinduism
[ ] The centuries-old conflict with Islam
[ ] The challenges of the contemporary world
[x] The changes imposed by past British colonial rule