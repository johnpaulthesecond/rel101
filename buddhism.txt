#Buddhism

# Where does Buddhism originate from?

[ ] Japan
[x] India
[ ] Europe
[ ] China


# Who became known as the Buddha?

[x] Siddartha Gautama
[ ] King Ashoka
[ ] Amitabha Buddha
[ ] the Dalai Lama


# What was the approach to life and spirituality taught by Buddha?

[x] the middle way - a path of moderation between self-indulgence and asceticsm
[ ] asceticism - a path of extereme abstinence
[ ] hedonism - a path of self-indulgence
[ ] the third way - a centrist alternative


# What was the order of monks and nuns started by the Buddha known as?

[x] the Sangha
[ ] the Dharma
[ ] the Bodhi
[ ] the Buddha


# What is not generally regarded as the basic core of Buddhism, or the Three Jewels?

[ ] the Buddha
[ ] the Dharma
[ ] the Sangha
[x] the Bodhi


# What is the Dharma?

[x] the sum total of Buddhist teachings about how to view the world and how to live properly
[ ] the community of monks and nuns
[ ] the three marks of reality
[ ] the noble eightfold path

# What is not one of the three marks of reality?

[ ] constant change
[ ] a lack of permanent identiy
[ ] the existence of suffering
[x] illusion


# What is the meaning of anichcha?

[x] life's constant change, or impermanence
[ ] no permanent identiy
[ ] suffering
[ ] inner peace


# What is the meaning of anatta?

[x] no permanent identity
[ ] life's constant change, or impermanence
[ ] suffering
[ ] inner peace


# What is the meaning of dukkha?

[x] suffering
[ ] life's constant change, or impermanence
[ ] inner peace
[ ] no pemanent identity


# Which is the first of the Four Noble Truths?

[x] To live is to suffer
[ ] suffering comes from desire
[ ] to end suffering, end desire
[ ] release from suffering is possible by following the Noble Eightfold Path


# Which is the second of the Four Noble Truths?

[x] Suffering comes from desire
[ ] to live is to suffer
[ ] to end suffering, end desire
[ ] release from suffering is possible by following the Noble Eightfold Path


# Which is the third of the Four Noble Truths?

[x] To end suffering, end desire
[ ] suffering comes from desire
[ ] release from suffering is possible by following the Noble Eightfold Path
[ ] to live is to suffer


# Which is the fourth of the Four Noble Truths?

[x] Release from suffering is possible by following the Noble Eightfold Path
[ ] suffering comes from desire
[ ] to live is to suffer
[ ] to end suffering, end desire

# What is not a meaning of the term Nirvana?

[ ] the end of suffering
[ ] inner peace
[ ] liberation from the limitations of the world
[x] enlightenment


# What does not describe the ideal of ahimsa?

[ ] ahimsa is fundamental
[ ] to cause suffering to any being is cruel and unnecessary
[ ] it is natural and satisfying for the individual to live with gentleness
[x] none of the above


# Who was instrumental to spreading Buddhism outside of India?

[x] King Ashoka
[ ] Siddartha Guatama
[ ] the Dalai Lama
[ ] the Bodhisattva


# What is not one of the great branches of Buddhism we now recognize?

[ ] Theravada Buddhism
[ ] Mahayana Buddhism
[ ] Vajrayana Buddhism
[ ] Zen Buddhism


# What distinguishes Therevada Buddhism?

[x] its goal of passing on the Buddha's teachings unchanged.
[ ] its incorporation of esoteric practices and beliefs
[ ] its emphasis that everyone, in addition to monks, can attain nirvana
[ ] its belief in a messianic future Buddha


# Which is not part of the Tripitaka?

[ ] the vinaya
[ ] the sutra
[ ] the abhidharma
[x] the bodhi


# What distinguishes Mahayana Buddhism?

[ ] its goal of passing on the Buddha's teachings unchanged.
[ ] its incorporation of esoteric practices and beliefs
[x] its emphasis that everyone, in addition to monks, can attain nirvana
[ ] its belief in a messianic future Buddha


# How did Mahayana Buddhism shift the notion of what is virtuous?

[x] It paired wisdom with compassion as central to its teachings.
[ ] it identified virtue as the mean between excess and deficiency
[ ] it could only be achieved by dedicated study
[ ] it was attained by following the middle way


# Which form of Buddhism holds karuna as an essential virtue?

[x] Mahayana Buddhism
[ ] Theravada Buddhism
[ ] Vajrayana Buddhism
[ ] Dharmakaya Buddhism


# What is a bodhisattva?

[x] a person who refuses to fully nirvana in order to be reborn on earth.
[ ] someone who is esteemed for detached wisdom and unwordly living
[ ] the Buddha reborn
[ ] a member of the sangha


# Which is not part of the trikaya doctrine?

[ ] Dharmakaya, the cosmic Buddha nature
[ ] Nirmanakaya, Siddhartha Guatama's phsyical body
[ ] Maitreya, a future buddha
[x] Mahayana, supernatural buddhas who live in the heavens


# What does not describe the Mahayana doctrine which asserts that all reality is shunyata?

[ ] everything is in constant change
[ ] each apparently individual entity is actually empty of any permanent individual identity.
[ ] the expereince that everything is a part of everything else
[x] the world is illusion; things are real but not in the way typically thought

# What is the notion of tathata?

[x] everyday events reveal the nature of reality
[ ] the nature of reality is revealed through enlightenment
[ ] the nature of reality is nirvana
[ ] meditation reveals the nature of reality


# What is the primary text of the specifically Mahayana written works?

[x] the Prajnaparamita Sutras
[ ] the Vimalakirti Sutra
[ ] the Sukhavati Vyuha Sutra
[ ] Saddharma Pundarika Sutra


# Which is not a major school of Mahayana Buddhism?

[x] Tibetan Buddhism
[ ] Zen Buddhism
[ ] Jodo Buddhism
[ ] Shingon Buddhism


# What distinguishes Vajrayana Buddhism?

[ ] its goal of passing on the Buddha's teachings unchanged.
[x] its incorporation of esoteric practices and beliefs
[ ] its emphasis that everyone, in addition to monks, can attain nirvana
[ ] its belief in a messianic future Buddha



# What is the most prominent form of Vajrayana Buddhism?

[x] Tibetan Buddhism
[ ] Shingon Buddhism
[ ] Nichiren Buddhism
[ ] Tendai Buddhism
